import csv
import json
import argparse



ap = argparse.ArgumentParser()
ap.add_argument('-r', "--replace", action='store_true',
	help="automatically replace the file in the source data")
ap.add_argument("-csl", "--csl", required=False, type= int, metavar="NUMBER",
	help="generate the dummy data in the real csl-json format. Give a number as parameter:"
         " 0 for csl-names,"
         " 1 for zotero-ui names,"
         " 2 for law-names,"
         " 3 for Beschreibung"
         " and 4 for Beispiel")

args = vars(ap.parse_args())
if args["csl"] is not None and args["replace"]:
    raise ValueError("you cannot select both arguments at the same time. For correct use, see -h")

#csv file has the format csl, zotero ui, law, beschreibung, beispiel
# a line starting with ! wont be converted to csl-json and will be used to add additional options to this object (see src/example data) (TODO)

#TODO include all fields
with open('translations.csv', newline='') as csvfile:
    reader = csv.reader(csvfile, delimiter=',', quotechar='"')
    current_object = {}
    objectlist = []
    new = True

    #if translationdata schould be generated
    if args["csl"] is None:
        for row in reader:

            #check empty line
            empty = True
            for el in row:
                if len(el)>0:
                    empty = False

            if empty:
                #if line is completely empty start new object
                objectlist.append(current_object)
                current_object = {}
                new = True

            elif new:
                #the first after empty is always the type
                new = False
                current_object["type"] = row

            elif len(row[2]) == 0:
                # if law is empty dont add it to the line
                continue

            elif row[0] == "issued":
                inside = {}
                inside["literal"] = row
                current_object[row[0]] = inside

            elif len(row[0]) > 0 and row[0][0] == "!":
                # not include into csl-json
                # TODO need to build additional options from this
                continue

            elif len(row[0]) > 0:
                #else add a normal line
                current_object[row[0]] = row
            else:
                raise IOError

    #if json data should be generated
    elif args["csl"] is not None:
        for row in reader:
            # needs to remenber the csl_attribute in case it is the type
            csl_attr = row[0]
            if args["csl"] >= len(row):
                raise ValueError("Your argument CSL is not in the supported interval")
            element = row[args["csl"]]

            # check empty line
            empty = True
            for el in row:
                if len(el) > 0:
                    empty = False

            if empty:
                # if line is completely empty start new object
                objectlist.append(current_object)
                current_object = {}
                new = True

            elif new:
                # the first after empty is always the type
                new = False
                current_object["type"] = csl_attr

            elif len(element) == 0:
                # if element is empty dont add it to the line
                continue

            elif row[0] == "issued":
                inside = {}
                inside["literal"] = element
                current_object[row[0]] = inside

            elif len(row[0]) > 0 and row[0][0] == "!":
                # not include into csl-json
                continue

            elif len(row[0]) > 0:
                # else add a normal line
                current_object[row[0]] = element
            else:
                raise IOError
#add last to objectlist
objectlist.append(current_object)

#standard file name
file = "tranlationData.js"

if args["replace"]:
    # if replace is active, then overwrite dummyData in the repo
    file = "../../src/dummyData.js"
if args["csl"] is None:
    #write the javascript fiel, including the json object
    with open(file, "w") as write_file:
        write_file.write("\"use strict\"\n")
        write_file.write("define(function () {\n"
                         "\tvar CSLEDIT_dummyData = {};\n\n")
        write_file.write("    /**\n"
                         "     * A Hard Coded List of all the relevant Typed of Citations (Book, Statute...) for the German Law\n"
                         "     * All Types listed here will be on the LawEditor Page in the Dragn'Drop Section\n"
                         "     * The Types conatain all relevant fields for the german law. The CSL-Name, Zotero-Name, Law-Name, Description are saved here.\n"
                         "     * For better understanding of what the fields are see @CSL_dummyData.LawNames, ...\n"
                         "     */\n")
        write_file.write("\tCSLEDIT_dummyData.jsonDocumentList =")
        json.dump(objectlist, write_file, indent=4)
        write_file.write(";\n")
        write_file.write("\treturn CSLEDIT_dummyData;\n")
        write_file.write("});")
else:
    #write only the json because csl is active
    with open("dummyCitations.json", "w") as write_file:
        json.dump(objectlist, write_file, indent=4)



