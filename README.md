This repo contains Data for the cslEditorLib. 

# convertToRepo.py

Using the ``translations.csv`` the Python Skript``convertToRepo.py`` generates the translation data in the necessary format. The user can decide if he wants pure csl-json data with the option ``-csl NUMBER`` or the translationData which will be in the same format as the file in the main repository under ``src/dummyData.js``. By giving the option ``-r`` or ``--replace`` this file will automatically be replaced by the new generated version (assumed the both repositories are cloned in the designated form). 

# translations.csv
This file contains differnet section marked by an empty line. Every sections provides data for the SourceType mentioned in the first row of this section. Every other row stands for a CSL-Field, used in this SourceType. Each colummn contains information about the field. The first colummn is the CSl-Identifier. The colummns are  in this order: 

1. CSL-Identifier
2. Label used in the Zotero-UI
3. The Label for the german law (in german language)
4. A description for this Field and for what it is designated
5. An Example

Not all colummns are filled in every row.

# typeMap

the files ``typeMap.css``, ``typeMap.xml`` and ``typeMap.xsl`` are taken from the project [z2csl](https://github.com/aurimasv/z2csl) (Zotero to Csl), commit [c604ab5b](https://github.com/aurimasv/z2csl/commit/c604ab5bab5e154a013536453142c14a3366dd34). These represent the whole mapping between Zotero fields and CSL-fields.

# translationData.js and DummyData.json

These are the output files, genrated using ``convertToRepo.py``
