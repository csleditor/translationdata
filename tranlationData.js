"use strict"
define(function () {
	var CSLEDIT_dummyData = {};

    /**
     * A Hard Coded List of all the relevant Typed of Citations (Book, Statute...) for the German Law
     * All Types listed here will be on the LawEditor Page in the Dragn'Drop Section
     * The Types conatain all relevant fields for the german law. The CSL-Name, Zotero-Name, Law-Name, Description are saved here.
     * For better understanding of what the fields are see @CSL_dummyData.LawNames, ...
     */
	CSLEDIT_dummyData.jsonDocumentList =[
    {
        "type": [
            "bill",
            "Bill",
            "Gesetzesentwurf",
            "Zitieren eines Gesetzentwurfs bzw. eines Gesetz\u00e4nderungsentwurf",
            ""
        ],
        "container-title": [
            "container-title",
            "Code",
            "Drucksache",
            "Die Drucksache unter der die \u00c4nderung ver\u00f6ffentlicht wurde",
            ""
        ],
        "issued": {
            "literal": [
                "issued",
                "Date",
                "Datum Ver\u00f6ffentlichung",
                "Datum der Ver\u00f6ffentlichung",
                ""
            ]
        },
        "note": [
            "note",
            "Extra",
            "\u00c4nderung / Entwurf",
            "Zur Unterscheidung zwischen Neu-Entwurf oder \u00c4nderung werden hier die passenden Informationen eingegeben.",
            " \u201cGesetzentwurf\u201d oder \u201cEntwurf zur Gesetzes\u00e4nderung\u201d"
        ],
        "references": [
            "references",
            "History",
            "Fundstelle",
            "Hier wird die Fundstelle angegeben. Gew\u00f6hnlich recht jedoch die Drucksache.",
            ""
        ],
        "title": [
            "title",
            "Title",
            "Gesetzestitel",
            "Titel des Gesetzes",
            ""
        ]
    },
    {
        "type": [
            "book",
            "Book",
            "Buch",
            "Hier werden alle Buchformen gesammelt, die nicht n\u00e4her definiert sind",
            ""
        ],
        "author": [
            "author",
            "Author",
            "Autor",
            "Der Autor des Buches",
            ""
        ],
        "editor": [
            "editor",
            "Editor",
            "Editor / Hrsg.",
            "Editor bzw. Herausgeber des Buches. (Bei Sammelb\u00e4nden sollte gepr\u00fcft werden ob eine Zitierung unter Handbuch / Kommentar / Festschrift sinnvoller ist!)",
            ""
        ],
        "issued": {
            "literal": [
                "issued",
                "Date",
                "Erscheinungsdatum",
                "",
                ""
            ]
        },
        "edition": [
            "edition",
            "Edition",
            "Auflage",
            "",
            ""
        ],
        "publisher-place": [
            "publisher-place",
            "Place",
            "Verlagsort",
            "",
            ""
        ],
        "publisher": [
            "publisher",
            "Publisher",
            "Verlag",
            "",
            ""
        ],
        "title-short": [
            "title-short",
            "Short Title",
            "Abk\u00fcrzung Titel",
            "Abk\u00fcrzung des Titels des Buches",
            ""
        ],
        "title": [
            "title",
            "Title",
            "Titel",
            "Titel des Buches",
            ""
        ]
    },
    {
        "type": [
            "entry-encyclopedia",
            "Encyclopedia Article",
            "Kommentar / Handbuch",
            "Hier werden Kommentare und Handb\u00fccher zitiert. Hinweis: Ein Kommentar unterscheidet sich in erster Linie von einem Handbuch indem er keinen Beitragstitel hat.",
            ""
        ],
        "author": [
            "author",
            "Author",
            "Autor Beitrag",
            "Der Autor des Beitrags",
            ""
        ],
        "editor": [
            "editor",
            "Editor",
            "Begr\u00fcnder & Herausgeber",
            "Herausgeber des Buches, ist der Begr\u00fcnder nicht Herausgeber, so sollte er trotzdem hier erw\u00e4hnt werden",
            ""
        ],
        "issued": {
            "literal": [
                "issued",
                "Date",
                "Erscheinungsdatum",
                "Das Datum an welchem die Arbeit erschienen ist. Kann sowohl ein vollst\u00e4ndiges Datum sein, als auch nur eine Jahreszahl enthalten",
                ""
            ]
        },
        "edition": [
            "edition",
            "Edition",
            "Auflage",
            "",
            ""
        ],
        "container-title": [
            "container-title",
            "Encyclopedia Title",
            "Buchtitel ",
            "Titel des Buches.",
            "M\u00fcnchener Kommentar zum BGB"
        ],
        "container-title-short": [
            "container-title-short",
            "In Extra: {:container-title-short: dein Kurztitel}",
            "Abk\u00fcrzung Buchtitel",
            "Abk\u00fcrzender Titel des Buches. Zur Eingabe in Zotero muss in das Extra Feld {:container-title-short: dein Kurztitel} eingegeben werden",
            "M\u00fcnKomm BGB"
        ],
        "note": [
            "note",
            "",
            "Paragraph /Artikel",
            "Hier wird der Paragraph festgehalten, wenn es sich um einen Kommentar handelt",
            ""
        ],
        "ISBN": [
            "ISBN",
            "ISBN",
            "ISBN",
            "",
            ""
        ],
        "language": [
            "language",
            "Language",
            "Sprache",
            "",
            ""
        ],
        "page": [
            "page",
            "Pages",
            "Seiten",
            "Anfang und Ende des Beitrags (handelt sich um ein Handbuch)",
            ""
        ],
        "publisher-place": [
            "publisher-place",
            "Place",
            "Verlagsort",
            "",
            ""
        ],
        "publisher": [
            "publisher",
            "Publisher",
            "Verlag",
            "",
            ""
        ],
        "title-short": [
            "title-short",
            "Short Title",
            "Abk\u00fcrzung Beitragstitel",
            "",
            ""
        ],
        "title": [
            "title",
            "Title",
            "Beitragstitel",
            "Titel des Beitrags (handelt es sich um ein Handbuch",
            ""
        ]
    },
    {
        "type": [
            "legal_case",
            "Case",
            "Urteil / Rechtsprechung",
            "Hier werden Urteile zitiert. Zwar ist es m\u00f6glich ein Urteil auch unter Artikel aus einer Zeitschrift zu zitieren, dieser Typ bietet jedoch bessere Unterst\u00fctzung",
            ""
        ],
        "authority": [
            "authority",
            "Court",
            "Gericht",
            "Das Gericht, dessen Beschluss zitiert wird.",
            ""
        ],
        "author": [
            "author",
            "Author",
            "Senat",
            "Der Senat der den zitierten Beschluss gefasst hat.",
            ""
        ],
        "issued": {
            "literal": [
                "issued",
                "Date Decided",
                "Entscheidungsdatum",
                "Das Datum, an welchem die Entscheidung gef\u00e4llt wurde",
                ""
            ]
        },
        "number": [
            "number",
            "Docket Number",
            "Aktenzeichen",
            "Das Aktenzeichen unter welchem die Entscheidung zu finden ist.",
            ""
        ],
        "container-title": [
            "container-title",
            "Reporter",
            "Zeitschrift / Fundstelle",
            "Die Fundstelle, welcher in der Regel die Zeitschrift ist, in welcher die Entscheidung ver\u00f6ffentlicht wurde",
            ""
        ],
        "volume": [
            "volume",
            "Reporter Volume",
            "Jahrgang und Ausgabe der Zeitschrift",
            "",
            "(12)99"
        ]
    },
    {
        "type": [
            "article-journal",
            "Journal Article",
            "juristische Zeitschrift",
            "Hier werden Artikel aus Zeitschriften zitiert, insbesondere aus juristischen Zeitschriften",
            ""
        ],
        "author": [
            "author",
            "Author",
            "Autor des Beitrags",
            "",
            ""
        ],
        "editor": [
            "editor",
            "Editor",
            "Herausgeber",
            "",
            ""
        ],
        "container-title-short": [
            "container-title-short",
            "Journal Abbr",
            "Abk\u00fcrzung Zeitschrift",
            "Die Abk\u00fcrzung des Namens der Zeitschrift.",
            "NjW"
        ],
        "language": [
            "language",
            "Language",
            "Sprache",
            "",
            ""
        ],
        "page": [
            "page",
            "Pages",
            "Seiten",
            "Beginn und Ende des Beitrags",
            ""
        ],
        "container-title": [
            "container-title",
            "Publication",
            "Name der Zeitschrift",
            "Der volle Name der Zeitschrift ( wird meist gar nicht ben\u00f6tigt)",
            "Neue Juristische Wochenschrift"
        ],
        "title": [
            "title",
            "Title",
            "Beitragstitel",
            "Titel des Beitrags",
            ""
        ],
        "volume": [
            "volume",
            "Volume",
            "Jahrgang und Ausgabe",
            "",
            "(12)99"
        ]
    },
    {
        "type": [
            "paper-conference",
            "Conference paper",
            "juristische Festschrift",
            "Hier werden Beitr\u00e4ge aus Festschriften zitiert",
            ""
        ],
        "author": [
            "author",
            "Author",
            "Autor des Beitrags",
            "",
            ""
        ],
        "collection-editor": [
            "collection-editor",
            "Series Editor",
            "Editor",
            "",
            ""
        ],
        "issued": {
            "literal": [
                "issued",
                "Date",
                "Datum Ver\u00f6ffentlichung",
                "",
                ""
            ]
        },
        "note": [
            "note",
            "Extra",
            "Anlass der Festschrift",
            "",
            ""
        ],
        "language": [
            "language",
            "Language",
            "Sprache",
            "",
            ""
        ],
        "page": [
            "page",
            "Pages",
            "Seiten",
            "Beginn und Ende des Beitrags",
            ""
        ],
        "container-title": [
            "container-title",
            "Proceedings Title",
            "Name der Festschrift",
            "Name der Festschrift (nicht des Beitrags)",
            ""
        ],
        "container-title-short": [
            "container-title-short",
            "In Extra: {:container-title-short: dein Kurztitel}",
            "Abk\u00fcrzung Festschrift Titel",
            "Abk\u00fcrzender Titel der Festschrift. Zur Eingabe in Zotero muss in das Extra Feld {:container-title-short: dein Kurztitel} eingegeben werden",
            "M\u00fcnKomm BGB"
        ],
        "publisher-place": [
            "publisher-place",
            "Place",
            "Ort der Institution",
            "",
            ""
        ],
        "publisher": [
            "publisher",
            "Publisher",
            "Name der Institution",
            "",
            ""
        ],
        "title": [
            "title",
            "Title",
            "Beitragstitel",
            "Titel des Beitrags",
            ""
        ]
    },
    {
        "type": [
            "thesis",
            "Thesis",
            "Dissertation",
            "",
            ""
        ],
        "author": [
            "author",
            "Author",
            "Autor",
            "",
            ""
        ],
        "issued": {
            "literal": [
                "issued",
                "Date",
                "Erscheinungsdatum",
                "",
                ""
            ]
        },
        "publisher-place": [
            "publisher-place",
            "Place",
            "Ort der Institution",
            "",
            ""
        ],
        "title-short": [
            "title-short",
            "Short Title",
            "Kurztitel",
            "",
            ""
        ],
        "title": [
            "title",
            "Title",
            "Titel",
            "",
            ""
        ],
        "publisher": [
            "publisher",
            "University",
            "Institution / Universit\u00e4t",
            "",
            ""
        ]
    },
    {
        "type": [
            "article-newspaper",
            "Newspaper Article",
            "Zeitungsartikel",
            "F\u00fcr Beitr\u00e4ge aus Zeitungen (z.B. Tages/Wochenzeitungen, nicht juristische Zeitschriften)",
            ""
        ],
        "author": [
            "author",
            "Author",
            "Autor",
            "",
            ""
        ],
        "issued": {
            "literal": [
                "issued",
                "Date",
                "Datum",
                "",
                ""
            ]
        },
        "page": [
            "page",
            "Pages",
            "Seiten",
            "Beginn und Ende des Beitrags",
            ""
        ],
        "publisher-place": [
            "publisher-place",
            "Place",
            "Verlagsort",
            "",
            ""
        ],
        "container-title": [
            "container-title",
            "Publication",
            "Name der Zeitung",
            "",
            ""
        ],
        "section": [
            "section",
            "Section",
            "Abschnitt",
            "",
            ""
        ],
        "title": [
            "title",
            "Title",
            "Titel des Artikels",
            "",
            ""
        ]
    },
    {
        "type": [
            "chapter",
            "Book Section",
            "Buchbeitrag / Kapitel",
            "",
            ""
        ],
        "abstract": [
            "abstract",
            "Abstract",
            "Abstract",
            "",
            ""
        ],
        "container-title": [
            "container-title",
            "Book Title",
            "Buchtitel",
            "",
            ""
        ],
        "author": [
            "author",
            "Author",
            "Autor des Beitrags",
            "",
            ""
        ],
        "container-author": [
            "container-author",
            "Book Author",
            "Autor des Buches",
            "",
            ""
        ],
        "editor": [
            "editor",
            "Editor",
            "Herausgeber",
            "",
            ""
        ],
        "collection-editor": [
            "collection-editor",
            "Series Editor",
            "Begr\u00fcnder",
            "",
            ""
        ],
        "issued": {
            "literal": [
                "issued",
                "Date",
                "Erscheinungsdatum",
                "",
                ""
            ]
        },
        "edition": [
            "edition",
            "Edition",
            "Auflage",
            "",
            ""
        ],
        "page": [
            "page",
            "Pages",
            "Seiten",
            "",
            ""
        ],
        "publisher-place": [
            "publisher-place",
            "Place",
            "Verlagsort",
            "",
            ""
        ],
        "publisher": [
            "publisher",
            "Publisher",
            "Verlag",
            "",
            ""
        ],
        "collection-title": [
            "collection-title",
            "Series",
            "Name der Reihe",
            "",
            ""
        ],
        "collection-number": [
            "collection-number",
            "Series Number",
            "Band innerhalb einer Reihe",
            "",
            ""
        ],
        "title-short": [
            "title-short",
            "Short Title",
            "Abk\u00fcrzung Beitragstitel",
            "",
            ""
        ],
        "title": [
            "title",
            "Title",
            "Beitragstitel",
            "",
            ""
        ]
    }
];
	return CSLEDIT_dummyData;
});